﻿using BackendAPI.Data;
using BackendAPI.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace FoodItemMicroservice.Repository
{
    public class FoodItemRepository : IFoodItemRepository
    {
        private readonly DataContext _context;
        public FoodItemRepository(DataContext context)
        {
            _context = context;
        }

        async public Task<List<FoodItems>> GetAllFoodItemsAsync()
        {
            return await _context.FoodItems.ToListAsync();
        }

        async public Task<FoodItems> GetFoodItemDetailsAsync(int id)
        {
            var data= await _context.FoodItems.FindAsync(id);
            return data;
        }
    }
}
   
