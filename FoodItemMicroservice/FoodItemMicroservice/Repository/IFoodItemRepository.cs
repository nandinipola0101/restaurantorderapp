﻿using BackendAPI.Models;

namespace FoodItemMicroservice.Repository
{
    public interface IFoodItemRepository
    {
        
        Task<List<FoodItems>> GetAllFoodItemsAsync();

        Task<FoodItems> GetFoodItemDetailsAsync(int id);

    }
}
