﻿using BackendAPI.Data;
using BackendAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace OrderMicroservice.Repository
{
    public class OrderRepository : IOrderRepository
    {
        private readonly DataContext _context;
        public OrderRepository(DataContext context)
        {
            this._context = context;
        }
        async public Task<List<Orders>> GetAllOrders()
        {
            return await _context.Orders.ToListAsync();
        }

        async public Task<List<Orders>> GetCurrentUserOrders(int id)
        {
            return await _context.Orders.Where(o => o.UserId == id).ToListAsync();
        }
    }
}
