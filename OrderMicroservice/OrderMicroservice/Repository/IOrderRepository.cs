﻿using BackendAPI.Models;

namespace OrderMicroservice.Repository
{
    public interface IOrderRepository
    {
        Task<List<Orders>> GetAllOrders();
        Task<List<Orders>> GetCurrentUserOrders(int id);
    }
}
